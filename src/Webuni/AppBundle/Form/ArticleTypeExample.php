<?php

namespace Webuni\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Webuni\AppBundle\Entity\Article;
use Webuni\AppBundle\Form\EventListener\AddAgreementFieldSubscriber;

/**
 * Description of TaskType
 *
 * @author Petr Jaša
 */
class ArticleTypeExample extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', array(
                'label' => 'Titulek',
//                'constraints' => array(
//                    new NotBlank(array('groups' => array('create'))),
//                )
//                'error_bubbling' => true,

            ))
            ->add('content', 'textarea', array(
                'validation_groups' => array('registration'),
//                'constraints' => array(
//                    new NotBlank(array('groups' => array('create', 'update'))),
//                    new Length(array('min' => 3)),
//                )
            ))
            ->add('publishAt', 'date', array(
            ))
//            ->add('emails', 'bootstrap_collection', array(
//                'type' => 'email',
//                'allow_add' => true,
//                'allow_delete' => false,
//                'add_button_text' => 'pridej',
//            ))
            ->add('save', 'submit')
            ->add('previousStep', 'submit', array(
                'validation_groups' => false //array('create')
            ))
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event){
            $form = $event->getForm();
            /** @var Article $article */
            $article = $event->getData();

            if (!$article->getId()) {
                $form->add('agreement', 'checkbox', array(
                    'mapped' => false
                ));
            }
        });
    }

    /**
     * @param FormView $view
     * @param FormInterface $form
     * @param array $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {

    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Webuni\AppBundle\Entity\Article',
            'validation_groups' => array('create'),
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
        ));

        $resolver->setDefaults(array(
            'validation_groups' => function(FormInterface $form) {
                /** @var Article $data */
                $article = $form->getData();
                if ($article->getId()) {
                    return array('update');
                } else {
                    return false; //array('create');
                }
            },
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'webuni_article';
    }
}