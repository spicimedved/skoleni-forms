<?php
namespace Webuni\AppBundle\Form\Handler;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Webuni\AppBundle\Entity\Article;


/**
 * Description of ArticleCreationHandler
 *
 * @author Petr Jaša
 */
class ArticleCreationHandler
{
    protected $entityManager;
    protected $secureRandom;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function handle(FormInterface $form, Request $request)
    {
        $form->handleRequest($request);
        if (!$form->isValid()) {
            return false;
        }
        $validArticle = $form->getData();
        $this->createArticle($validArticle);

        return true;
    }

    private function createArticle(Article $article)
    {
        $this->entityManager->persist($article);

        // pozor na flush
        // pouzijte ho zde, jen kdyz vite, co delate
        // jinak se doporucuje ho volat az v akci kontroleru
        $this->entityManager->flush();
    }
}