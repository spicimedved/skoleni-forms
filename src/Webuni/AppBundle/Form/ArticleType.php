<?php

namespace Webuni\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Webuni\AppBundle\Entity\Article;

/**
 * Description of TaskType
 *
 * @author Petr Jaša
 */
class ArticleType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', array(
                'label' => 'Titulek',
            ))
            ->add('content', 'textarea')
            ->add('publishAt', 'date')
            ->add('save', 'submit')
        ;
    }

    /**
     * @param FormView $view
     * @param FormInterface $form
     * @param array $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
//        $view->vars['custom_var'] = 'My custom variable';
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Webuni\AppBundle\Entity\Article',
            'validation_groups' => function(FormInterface $form) {
                /** @var Article $article */
                $article = $form->getData();

                if ($article->getId()) {
                    return array('update');
                } else {
                    return array('create');
                }
            },
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'webuni_article';
    }
}