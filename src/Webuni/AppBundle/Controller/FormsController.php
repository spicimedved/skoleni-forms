<?php

namespace Webuni\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Webuni\AppBundle\Entity\Article;

/**
 * Description of FormsController
 *
 * @Route("/forms")
 *
 * @author Petr Jaša
 */
class FormsController extends Controller
{
    /**
     * @Route("/inline", name="forms.inline")
     * @Template()
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('title', 'text', array(
                'label' => 'Titulek',
                'error_bubbling' => true,
                'constraints' => array(
                    new NotBlank(), new Length(array(
                        'min' => '3'
                    ))
                )
            ))
            ->add('content', 'textarea', array(
                'label' => 'Obsah',
            ))
            ->add('publishAt', 'date', array(
                'widget' => 'single_text',
            ))
            ->add('save', 'submit', array(
                'label' => 'Save it!'
            ))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isValid()) {
            dump($form->getData());
        }

        return array(
            'form' => $form->createView()
        );
    }

    /**
     * @Route("/inline-pre-filled", name="forms.inline-filled")
     * @Template("WebuniAppBundle:Forms:index.html.twig")
     */
    public function inlinePreFilledAction()
    {
        $form = $this->createFormBuilder(array(
            'title' => 'Petr',
            'content' => 'content',
            'datum' => new \DateTime(),
        ))
            ->setAction($this->generateUrl('webuni_admin_default_inlineprefilled'))
            ->setMethod('GET')
            ->add('title', 'text', array(
                'label' => 'Titulek',
                'label_attr' => array(
                    'class' => 'special-label',
                    'data-role' => 'label'
                ),
//                'empty_data' => 'empty',
                'required' => false,
//                'data' => 'titulek',
                'trim' => true,
                'read_only' => false,
                'disabled' => false,
                'error_bubbling' => false,
                'error_mapping' => array(

                ),
//                'mapped' => true,
                'attr' => array(
                    'class' => 'text-field'
                ),
//                'property_path' => '[user_name]'
            ))
            ->add('content', 'textarea', array(
                'label' => 'Obsah',
            ))
            ->add('publishAt', 'date', array(
                'widget' => 'single_text',
            ))
            ->add('save', 'submit')
            ->getForm();

        return array(
            'form' => $form->createView()
        );
    }

    /**
     * @Route("/type", name="forms.type")
     * @Template("WebuniAppBundle:Forms:index.html.twig")
     */
    public function formTypeFormAction()
    {
        return array(
            'form' => $this->createForm('webuni_article', new Article())->createView()
        );
    }

    /**
     * @Route("/add-article", name="forms.add-article")
     * @Template("WebuniAppBundle:Forms:index.html.twig")
     *
     * @param Request $request
     * @return array
     */
    public function addArticleAction(Request $request)
    {
        $article = new Article();
        $form = $this->createForm('webuni_article', $article);

//        $form->get('publishAt')->getData();
//        $article = $form->getData();

        $form->handleRequest($request);
        if ($form->isValid()) {
            if ($form->get('save')->isClicked()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($article);
                $em->flush();
                return $this->redirect($this->generateUrl('webuni_app_forms_success'));
            }
        }

        return array(
            'form' => $form->createView()
        );
    }

    /**
     * @Route("/add-article-clean", name="forms.add-article-clean")
     * @Template("WebuniAppBundle:Forms:index.html.twig")
     *
     * @param Request $request
     * @return array
     */
    public function cleanFormAction(Request $request)
    {
        $form = $this->createForm('webuni_article', new Article());
        $formHandler = $this->get('webuni_admin.article_form_handler');

        if ($formHandler->handle($form, $request)) {
            // send confirmation message
            return $this->redirect($this->generateUrl('webuni_app_forms_success'));
        }

        return array(
            'form' => $form->createView()
        );
    }

    /**
     * @Route("/validation")
     * @Template()
     *
     * @return Response
     */
    public function validationAction()
    {
        $article = new Article();
        $article->setTitle('');
        $article->setContent('d');

        $validator = $this->get('validator');
        $errors = $validator->validate($article);

//        if (count($errors) > 0) {
//            $errorsString = (string) $errors;
//            return new Response($errorsString);
//        }
//        return new Response('Article je validní.');

        return array(
            'errors' => $errors,
            'errorCount' => count($errors)
        );
    }

    /**
     * @Route("/success.{_format}", defaults={"_format"="html"})
     * @Template("WebuniAppBundle:Default:success.html.twig")
     *
     * @return Response
     */
    public function successAction()
    {
        return array(
            'message' => 'Success'
        );
//        return new Response('Success');
    }
}
