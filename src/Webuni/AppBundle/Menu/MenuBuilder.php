<?php

namespace Webuni\AppBundle\Menu;

use Webuni\AppBundle\Event\MenuCreatedEvent;
use Webuni\AppBundle\Event\MenuEvent;
use Knp\Menu\FactoryInterface;
use Knp\Menu\Matcher\Matcher;
use Knp\Menu\Matcher\MatcherInterface;
use Knp\Menu\Matcher\Voter\UriVoter;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;


/**
 * Description of MenuBuilder
 *
 * @author Petr Jaša
 */
class MenuBuilder
{
    /** @var FactoryInterface */
    protected $factory;

    /** @var EventDispatcherInterface */
    protected $dispatcher;

    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;

    /**
     * @param FactoryInterface $factory
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(
        FactoryInterface $factory, AuthorizationCheckerInterface $authorizationChecker, EventDispatcherInterface $dispatcher
    ) {
        $this->factory = $factory;
        $this->dispatcher = $dispatcher;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param RequestStack $requestStack
     * @return \Knp\Menu\ItemInterface
     */
    public function createMainMenu(RequestStack $requestStack)
    {
        // set current uri
//        $request = $requestStack->getCurrentRequest();
//        $this->matcher->addVoter(new UriVoter($request->getRequestUri()));
//        dump('builder: creating menu');

        // create menu
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttributes(array('class' => 'nav navbar-nav'));

        // Homepage
        $menu->addChild('navigation.homepage', array('route' => 'homepage'));

        // Forms
        $menu->addChild('navigation.forms.inline', array('route' => 'forms.inline'));
        $menu->addChild('navigation.forms.add_article', array('route' => 'forms.add-article-clean'));

        // notify that menu was created
        $this->dispatcher->dispatch(MenuEvent::onMenuCreated, new MenuCreatedEvent($menu));

        return $menu;
    }
}
