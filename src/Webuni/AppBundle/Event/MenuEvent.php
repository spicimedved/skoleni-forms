<?php

namespace Webuni\AppBundle\Event;

/**
 * Description of MenuEvent
 *
 * @author Petr Jaša
 */
final class MenuEvent
{
    /**
     * The menu.created event is thrown each time an menu is created
     * in the system.
     *
     * @var string
     */
    const onMenuCreated = 'menu.main_created';
}
