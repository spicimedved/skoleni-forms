<?php

namespace Webuni\AppBundle\Event;

use Knp\Menu\ItemInterface;
use Knp\Menu\MenuItem;
use Symfony\Component\EventDispatcher\Event;

/**
 * Description of MenuCreatedEvent
 *
 * @author Petr Jaša
 */
class MenuCreatedEvent extends Event
{
    /** @var MenuItem */
    private $menu;

    /**
     * @param ItemInterface $menu
     */
    public function __construct(ItemInterface $menu)
    {
        $this->menu = $menu;
    }

    /**
     * @return MenuItem
     */
    public function getMenu()
    {
        return $this->menu;
    }
}
