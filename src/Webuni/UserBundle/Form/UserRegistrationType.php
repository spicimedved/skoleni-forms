<?php

namespace Webuni\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Webuni\UserBundle\Form\EventListener\AddAgreementFieldSubscriber;


class UserRegistrationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', 'text', array(
                'label' => 'user.firstname'
            ))
            ->add('username', 'text', array(
                'label' => 'user.username',
                'data' => $options['last_username'],
                'empty_data' => '',
                'attr' => array(
                    'placeholder' => 'zadejte login'
                )
            ))
            ->add('password', 'password', array(
                'label' => 'user.password'
            ))
            ->add('save', 'submit')
        ;

        $builder->addEventSubscriber(new AddAgreementFieldSubscriber());
    }

    /**
     * @param FormView $view
     * @param FormInterface $form
     * @param array $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
//        $view->vars['custom_var'] = 'My custom variable';
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Webuni\UserBundle\Entity\User',
            'last_username' => ''
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'webuni_user_registration';
    }
}