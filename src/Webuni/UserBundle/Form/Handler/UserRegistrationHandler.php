<?php
namespace Webuni\UserBundle\Form\Handler;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Webuni\UserBundle\Entity\User;


/**
 * Description of UserRegistrationHandler
 *
 * @author Petr Jaša
 */
class UserRegistrationHandler
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * @param EntityManager $entityManager
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(EntityManager $entityManager, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
    }

    public function handle(FormInterface $form, Request $request)
    {

        $form->handleRequest($request);
        if (!$form->isValid()) {
            return false;
        }
        $validUser = $form->getData();
        $this->createUser($validUser);

        return true;
    }

    private function createUser(User $user)
    {
        $user->setPassword(
            $this->passwordEncoder->encodePassword($user, $user->getPassword())
        );

        $this->entityManager->persist($user);

        // pozor na flush
        // pouzijte ho zde, jen kdyz vite, co delate
        // jinak se doporucuje ho volat az v akci kontroleru
        $this->entityManager->flush();
    }
}