<?php
namespace Webuni\UserBundle\Form\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints\True;
use Webuni\UserBundle\Entity\User;

/**
 * Description of AddAgreementFieldSubscriber
 *
 * @author Petr Jaša
 */
class AddAgreementFieldSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return array(FormEvents::PRE_SET_DATA => 'preSetData');
    }

    public function preSetData(FormEvent $event)
    {
        $form = $event->getForm();
        /** @var User $user */
        $user = $event->getData();

        if (!$user || null === $user->getId()) {
            $form->add('agreement', 'checkbox', array(
                'mapped' => false,
                'constraints' => array(
                    new True(),
                )
            ));
        }
    }
}