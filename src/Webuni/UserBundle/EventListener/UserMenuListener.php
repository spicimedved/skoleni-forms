<?php
namespace Webuni\UserBundle\EventListener;

use Webuni\AppBundle\Event\MenuCreatedEvent;
use Webuni\AppBundle\Event\MenuEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;


/**
 * Description of UserMenuListener
 *
 * @author Petr Jaša
 */
class UserMenuListener implements EventSubscriberInterface
{
    /**
     * @param MenuCreatedEvent $event
     */
    public function onMenuCreate(MenuCreatedEvent $event)
    {
        $menu = $event->getMenu();
        $menu->addChild('navigation.registration', array('route' => 'user.registration'));
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            MenuEvent::onMenuCreated => 'onMenuCreate'
        );
    }

}