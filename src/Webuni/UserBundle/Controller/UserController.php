<?php

namespace Webuni\UserBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Webuni\UserBundle\Entity\User;

/**
 * Description of UserController
 *
 *@Route("/user")
 *
 * @author Petr Jaša
 */
class UserController extends Controller
{
    /**
     * @Template()
     * @Route("/registration", name="user.registration")
     * @param Request $request
     * @return Response
     */
    public function registrationAction(Request $request)
    {
        $formHandler = $this->get('webuni_admin.user_registration_handler');
        $form = $this->createForm('webuni_user_registration', new User());

        if ($formHandler->handle($form, $request)) {
            return $this->redirect($this->generateUrl('webuni_user_user_success'));
        }

        return array(
            'form' => $form->createView()
        );
    }

    /**
     * @Route("/success")
     */
    public function successAction()
    {
        return new Response('<body>Success</body>');
    }
}
