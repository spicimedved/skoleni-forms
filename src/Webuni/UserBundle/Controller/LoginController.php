<?php

namespace Webuni\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\FormError;

/**
 * Description of LoginController
 *
 * @author Petr Jaša
 */
class LoginController extends Controller
{
    /**
     * @Route("/login", name="login")
     * @Template()
     */
    public function loginAction()
    {
        // pokud chceme zobrazit chybu behem prihlasovani, vyuzijeme authentication helper
        $helper = $this->get('security.authentication_utils');

        // submit musi smerovat na login_check routu, aby se pri odesilani
        // prihlasovacich udaju aktivoval UsernamePasswordFormAuthenticationListener
        $form = $this->createForm('webuni_user_login', null, array(
            'action' => $this->generateUrl('login_check'),
            'last_username' => $helper->getLastUsername()
        ));

        // chyby vzniklé mimo formulář je možné formuláři podstrčit
        //$form->addError(new FormError($helper->getLastAuthenticationError()));

        return array(
            'form'          => $form->createView(),
            'last_username' => $helper->getLastUsername(),
            'error'         => $helper->getLastAuthenticationError(),
        );
    }
}
